#![warn(clippy::pedantic)]
#![allow(unused, dead_code)]
#![allow(clippy::missing_errors_doc, clippy::missing_panics_doc)] // TODO temporary

pub mod partitioner;
mod utils;

use partitioner::Partitioner;
use rand::seq::SliceRandom;
use std::io::{Read, Write};
use std::iter::Iterator;

/// Write a full 'shuffled' stream to `output`.
///
/// All data in `prttnr`'s source will be written to output, but the parts will
/// be written in a random order.
///
/// The data written to `output` is in the following format (bit endian):
///     `num_parts` (8 bits) | `part_size` (64 bits) | `source_size` (64 bits) |
///     {`index` (8 bits) | data (`part_size` * 8 bits)} for `index` in `0..numparts`.
pub fn write_shuffled<R, W>(prttnr: &mut Partitioner<R>, output: &mut W) -> std::io::Result<()>
where
    R: Read,
    W: Write,
{
    output.write_all(&prttnr.get_stream_header())?;
    for index in generate_random_order(prttnr.num_parts) {
        output.write_all(&[index])?;
        output.write_all(prttnr.get_part(index)?)?;
    }
    Ok(())
}

/// Returns an iterator that will yield all indices in range `0..num_parts`,
/// but in a pseudorandom order.
fn generate_random_order(num_parts: u8) -> impl Iterator<Item = u8> {
    let mut x: Vec<u8> = (0..num_parts).collect();
    x.shuffle(&mut rand::thread_rng());
    x.into_iter()
}
