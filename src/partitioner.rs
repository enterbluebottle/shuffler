use std::io::Read;

use crate::utils;

#[derive(std::fmt::Debug)]
pub struct Partitioner<R: Read> {
    pub(crate) num_parts: u8,
    source: R,
    source_size: usize,
    part_size: usize,
    parts: Vec<Vec<u8>>,
    up_to: u8,
}

impl<R: Read> Partitioner<R> {
    fn new(source: R, source_size: usize) -> Result<Self, &'static str> {
        let (num_parts, part_size) = calculate_split(source_size)?;
        Ok(Partitioner {
            source,
            source_size,
            num_parts,
            part_size,
            parts: vec![vec![0; part_size]; num_parts.into()],
            up_to: 0,
        })
    }

    /// Return the part that corresponds to `index`. If we have not previously
    /// fetched this part we may try to read it from the Partitioner source.
    pub fn get_part(&mut self, index: u8) -> Result<&[u8], std::io::Error> {
        if index >= self.num_parts {
            return Err(std::io::Error::new(
                std::io::ErrorKind::Other,
                "index out of bounds",
            ));
        }

        // Read in data for that part (and all parts before it)
        if index >= self.up_to {
            for part_idx in (self.up_to..=index) {
                if part_idx == self.num_parts - 1 {
                    // Last part, can't expect full data.
                    let padding = self.num_parts as usize * self.part_size - self.source_size;
                    self.source.read_exact(
                        &mut self.parts[part_idx as usize][..(self.part_size - padding)],
                    )?;
                } else {
                    self.source.read_exact(&mut self.parts[part_idx as usize])?;
                }
            }
            self.up_to = index + 1;
        }

        // TODO: add index byte to front
        // PROBLEM: can't just prepend index - it won't live long enough.
        // Is there a way to do this without a copy?

        Ok(&self.parts[index as usize])
    }

    /// Returns the header that should be attached to an output stream from the
    /// partitioner. It will include metadata that will help a consumer piece
    /// together the original file.
    ///
    /// The header contains the following data packed in a big endian format:
    /// `num_parts` (8 bits) | `part_size` (64 bits) | `source_size` (64 bits)
    ///
    /// `part_size` and `source_size` are given in bytes.
    pub fn get_stream_header(&self) -> [u8; 17] {
        let mut header = [0; 17];
        header[0] = self.num_parts;

        let mut start: usize = 1 + (64 - usize::BITS as usize) / 8;
        header[start..9].copy_from_slice(&self.part_size.to_be_bytes());
        start += 8;
        header[start..].copy_from_slice(&self.source_size.to_be_bytes());
        header
    }
}

#[allow(clippy::cast_possible_truncation)]
/// Calculates how many parts to split source content of `source_size` bytes
/// into (`u8`), and how big each of those parts will be (`usize`).
///
/// We aim to split the source into parts of approximately 10,000 bytes
/// (except for very small or very large sources).
///
/// # Errors
/// Will return `Err` if the source content is too small to be split into parts.
pub fn calculate_split(source_size: usize) -> Result<(u8, usize), &'static str> {
    if source_size < 2 {
        return Err("source too small");
    }

    let num_parts: u8 = match utils::ceil_division(source_size, 10000) {
        0..=1 => 2,
        256..=usize::MAX => 255,
        n => n as u8,
    };

    let part_size = utils::ceil_division(source_size, usize::from(num_parts));

    Ok((num_parts, part_size))
}

#[cfg(test)]
mod tests {
    use super::*;
    use test_case::test_case;

    #[test_case(0 => matches Err(_); "source size 0 (too small)")]
    #[test_case(1 => matches Err(_); "source size 1 (too small)")]
    #[test_case(2 => matches Ok((2, 1)); "smallest possible source")]
    #[test_case(3 => matches Ok((2, 2)); "tiny source unevenly divisible by num_parts")]
    #[test_case(20_000 => matches Ok((2, 10_000)); "largest part size with only 2 parts")]
    #[test_case(20_001 => matches Ok((3, 6_667)); "split into 3 parts")]
    #[test_case(99_999 => matches Ok((10, 10_000)); "small file")]
    #[test_case(456_789 => matches Ok((46, 9_931)); "small-medium file")]
    #[test_case(1_123_456 => matches Ok((113, 9_943)); "medium file")]
    #[test_case(25_500_000 => matches Ok((255, 100_000)); "max num_parts for target part_size")]
    #[test_case(25_500_001 => matches Ok((255, 100_001)); "part_size grows once reach num_parts")]
    #[test_case(1_000_000_000 => matches Ok((255, 3_921_569)); "big file")]
    fn test_calculate_split(source_size: usize) -> Result<(u8, usize), &'static str> {
        calculate_split(source_size)
    }

    // TODO: use different readers
    #[test_case(std::io::empty(), 2, 2, 1)]
    #[test_case(std::io::empty(), 99_999, 10, 10_000)]
    fn test_new_partitioner<R: Read>(
        source: R,
        source_size: usize,
        want_num_parts: u8,
        want_part_size: usize,
    ) {
        let got = Partitioner::new(source, source_size).unwrap();

        // TODO: how to check that `source` has been set properly?

        assert_eq!(got.num_parts, want_num_parts, "got num_parts != wanted");
        assert_eq!(got.part_size, want_part_size, "got part_size != wanted");

        assert!(
            got.parts.len() == want_num_parts as usize,
            "parts vec does not have space for enough parts"
        );
        for (idx, part) in got.parts.into_iter().enumerate() {
            assert!(
                part.len() == want_part_size,
                "Part {} is {} bytes long, should be {}",
                idx,
                part.len(),
                want_part_size
            );
        }
    }

    #[test]
    fn test_get_part() {
        let input: [u8; 7] = [0, 1, 2, 3, 4, 5, 6];

        let mut partitioner = Partitioner {
            source: input.as_ref(),
            source_size: 7,
            num_parts: 4,
            part_size: 2,
            parts: vec![vec![0; 2]; 4],
            up_to: 0,
        };

        // We'll fill this in throughout the test
        let mut expected = vec![vec![0; 2]; 4];
        assert_eq!(partitioner.parts, expected, "initial `partitioner.parts`");

        // Read 1st part
        expected[0] = vec![0, 1];
        let got0 = partitioner.get_part(0).expect("on `get_part(0)`");
        assert_eq!(got0, expected[0], "part 0");
        assert_eq!(
            partitioner.parts, expected,
            "`partitioner.parts` after got part 0"
        );
        assert_eq!(partitioner.up_to, 1, "`up_to` not set correctly");

        // Skip a part: it will be read into the internal vec but not returned.
        expected[1] = vec![2, 3];
        expected[2] = vec![4, 5];
        let got2 = partitioner
            .get_part(2)
            .expect("on `get_part(2)` (skip a part)");
        assert_eq!(got2, expected[2], "part 2");
        assert_eq!(
            partitioner.parts, expected,
            "`partitioner.parts` after got part 2 (skipping part 1)"
        );
        assert_eq!(partitioner.up_to, 3, "`up_to` not set correctly");

        // Ask for a part that we already have
        let got1 = partitioner
            .get_part(1)
            .expect("on `get_part(1)` (already have)");
        assert_eq!(got1, expected[1], "part 1");
        assert_eq!(
            partitioner.parts, expected,
            "`partitioner.parts` after got part 1 (no read: should be unchanged since last call)"
        );
        assert_eq!(
            partitioner.up_to, 3,
            "`up_to` not set correctly (no read: should be unchanged since last call)"
        );

        // Get final part: padding added with no error.
        expected[3] = vec![6, 0];
        let got3 = partitioner
            .get_part(3)
            .expect("on `get_part(3)` (want zero padding)");
        assert_eq!(got3, expected[3], "part 3");
        assert_eq!(
            partitioner.parts, expected,
            "`partitioner.parts` after got part 3 (expect last byte to be zero padding)"
        );
        assert_eq!(partitioner.up_to, 4, "`up_to` not set correctly");

        // Error when ask for an index that's too big.
        partitioner.get_part(4).unwrap_err();
        assert_eq!(
            partitioner.parts, expected,
            "`partitioner.parts` should be unchanged after error"
        );
        assert_eq!(
            partitioner.up_to, 4,
            "`up_to` should be unchanged after error"
        );
    }

    #[test]
    /*TODO need fake reader. Make sure test UnexpectedEof on non-final part*/
    fn test_get_part_error_during_read() {}

    #[test]
    fn test_new_partitioner_fails_for_impossible_source_sizes() {
        let source: &[u8] = &[0];
        let got = Partitioner::new(source, 1);
        got.unwrap_err();
    }

    #[test_case(0, 0, 0 => [0x0; 17]; "all zeros")]
    #[test_case(1, 1, 1 => [0x01,
                            0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x01,
                            0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x01]
                            ; "check big endian")]
    #[test_case(113, 9_943, 1_123_456 => [0x71,
                            0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x26, 0xd7,
                            0x0, 0x0, 0x0, 0x0, 0x0, 0x11, 0x24, 0x80]
                            ; "realistic-ish values")]
    fn test_get_stream_header(num_parts: u8, part_size: usize, source_size: usize) -> [u8; 17] {
        let partitioner = Partitioner {
            source: std::io::empty(),
            source_size,
            num_parts,
            part_size,
            parts: vec![],
            up_to: 0,
        };

        partitioner.get_stream_header()
    }
}
