use num_traits::int::PrimInt;

/// Returns ceil(`numerator` / `divisor`).
///
/// # Panics
/// Panics if `divisor == 0`.
pub fn ceil_division<T>(numerator: T, divisor: T) -> T
where
    T: PrimInt, // TODO: Do I really need a whole crate just to do this?
{
    // TODO: can't write (n, 0) as the first pattern because it doesn't like
    // the call to the associated function. Surely there must be a better way
    // to get a 0 as a PrimInt?
    match (numerator / divisor, numerator % divisor == T::zero()) {
        (n, true) => n,
        (n, false) => n + T::one(),
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use test_case::test_case;

    #[test_case(2, 2 => 1; "evenly divisible")]
    #[test_case(1, 2 => 1; "uevenly divisible rounds up")]
    #[test_case(0, 2 => 0; "numerator zero works")]
    #[test_case(2, 0 => panics "divide by zero"; "divide by zero")]
    #[test_case(123_456, 321 => 385; "non-trivial example")]
    fn test_ceil_division_for_usize(numerator: usize, divisor: usize) -> usize {
        ceil_division(numerator, divisor)
    }

    #[test_case(2, 2 => 1; "evenly divisible")]
    #[test_case(1, 2 => 1; "uevenly divisible rounds up")]
    #[test_case(0, 2 => 0; "numerator zero works")]
    #[test_case(2, 0 => panics "divide by zero"; "divide by zero")]
    #[test_case(255, 31 => 9; "non-trivial example")]
    fn test_ceil_division_for_u8(numerator: u8, divisor: u8) -> u8 {
        ceil_division(numerator, divisor)
    }
}
